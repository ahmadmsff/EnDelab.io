<!DOCTYPE html>
<html>
<head>
	<title class="">EnDe - Multi Encoder & Decoder</title>
	<link href="assets/css/bootstrap.min.css" type="text/css" rel="stylesheet" media="screen">
	<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">
	<style type="text/css" media="screen">
		.none {
			display: none;
		}
		body {
			font-family: "Open Sans", Helvetica, sans-serif;
			padding-top: 50px;
		}
		.typed-cursor{
	    opacity: 1;
	    -webkit-animation: blink 0.7s infinite;
	    -moz-animation: blink 0.7s infinite;
	    animation: blink 0.7s infinite;
		}
		@keyframes blink{
		    0% { opacity:1; }
		    50% { opacity:0; }
		    100% { opacity:1; }
		}
		@-webkit-keyframes blink{
		    0% { opacity:1; }
		    50% { opacity:0; }
		    100% { opacity:1; }
		}
		@-moz-keyframes blink{
		    0% { opacity:1; }
		    50% { opacity:0; }
		    100% { opacity:1; }
		}
	</style>
	<style type="text/css" media="screen">
		.cuadro_intro_hover{
        padding: 0px;
		position: relative;
		overflow: hidden;
		height: 200px;
		}
		.cuadro_intro_hover:hover .caption{
			opacity: 1;
			transform: translateY(-150px);
			-webkit-transform:translateY(-150px);
			-moz-transform:translateY(-150px);
			-ms-transform:translateY(-150px);
			-o-transform:translateY(-150px);
		}
		.cuadro_intro_hover img{
			z-index: 4;
		}
		.cuadro_intro_hover .caption{
			position: absolute;
			top:150px;
			-webkit-transition:all 0.3s ease-in-out;
			-moz-transition:all 0.3s ease-in-out;
			-o-transition:all 0.3s ease-in-out;
			-ms-transition:all 0.3s ease-in-out;
			transition:all 0.3s ease-in-out;
			width: 100%;
		}
		.cuadro_intro_hover .blur{
			background-color: rgba(0,0,0,0.7);
			height: 300px;
			z-index: 5;
			position: absolute;
			width: 100%;
		}
		.cuadro_intro_hover .caption-text{
			z-index: 10;
			color: #fff;
			position: absolute;
			height: 300px;
			text-align: center;
			top:-20px;
			width: 100%;
		}
	</style>
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">EnDe</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse navbar-right">
          <ul class="nav navbar-nav">
            <li id="linkBase64" class="active"><a href="#base64">Base64</a></li>
            <li id="linkUri"><a href="#url">Url</a></li>
            <li id="linkAbout" data-target="#modalAbout" data-toggle="modal"><a href="#about">About</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
	</nav>
	<div class="container">
		<div class="header text-center" style="margin: 20px;">
			<h3>Type Your <span class="element"></span> Code</h3>
		</div>
		<!-- Base64 Form -->
		<div id="base64">
			<div class="col-md-12">
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							Encode
						</div>
						<div class="panel-body">
							<textarea class="form-control" id="inputEncodeBase64" rows="3"></textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							Decode
						</div>
						<div class="panel-body">
							<textarea class="form-control" id="inputDecodeBase64" rows="3"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="col-md-12" id="outEncodeBase64">
					<div class="panel panel-default">
						<div class="panel-heading">
							Output
						</div>
						<div class="panel-body">
							<textarea class="form-control" id="outputEncodeBase64" rows="5"></textarea>
						</div>
					</div>
				</div>
				<div class="col-md-12 hidden" id="outDecodeBase64">
					<div class="panel panel-default">
						<div class="panel-heading">
							Output
						</div>
						<div class="panel-body">
							<textarea class="form-control" id="outputDecodeBase64" rows="5"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Base64 Form -->
		<!-- URL Form -->
		<div id="uri" class="hidden">
			<div class="col-md-12">
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							Encode
						</div>
						<div class="panel-body">
							<textarea class="form-control" id="inputEncodeUrl" rows="3"></textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							Decode
						</div>
						<div class="panel-body">
							<textarea class="form-control" id="inputDecodeUrl" rows="3"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="col-md-12" id="outEncodeUrl">
					<div class="panel panel-default">
						<div class="panel-heading">
							Output
						</div>
						<div class="panel-body">
							<textarea class="form-control" id="outputEncodeUrl" rows="5"></textarea>
						</div>
					</div>
				</div>
				<div class="col-md-12 hidden" id="outDecodeUrl">
					<div class="panel panel-default">
						<div class="panel-heading">
							Output
						</div>
						<div class="panel-body">
							<textarea class="form-control" id="outputDecodeUrl" rows="5"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End URL Form -->
	</div>
	<!-- Modal -->
	<div class="modal fade" id="modalAbout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">About EnDe</h4>
	      </div>
	      <div class="modal-body">
	        EnDe adalah webtool untuk keperluan Encode dan Decoding.<br>
	        Kamu bisa membantu owner dengan cara <a href="#donate" title="Donasi" class="btn btn-sm btn-primary">Donasi</a><br><br><br>
	        <center>Created By AhmadMsff With <i class="fa fa-heart"></i></center>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- End Modal -->
<!-- Start Footer -->
    <footer class="navbar navbar-fixed-bottom">
	<hr>
	    <div class="container">
		    <div class="col-md-12">
		    	<div class="col-md-6">
			    	<div class="row">
			    		<div class="text-center">
		    				<p href="" title="">Copyright &copy; <a href="" title="EnDe - Multi Encoder & Decoder">EnDe</a> All Rightreserved.</p>
		    			</div>
			    	</div>
	    		</div>
	    		<div class="col-md-6">
			    	<div class="row">
			    		<div class="text-center">
			        		<p href="" title="">Design With <i class="fa fa-heart"></i> By <a href="">AhmadMsff</a>.</p>
			        	</div>
			    	</div>
		       	</div>
		    </div>
		</div>
    </footer>
<!-- End Footer -->

<!-- Start Script -->
<script src="assets/js/jQuery3.2.0.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/jQuery-Base64.js" type="text/javascript"></script>
<script src="assets/js/typed.min.js" type="text/javascript"></script>
<script>        
        $(document).ready(function(){
            $(function(){
                $(".element").typed({
                    strings: ["^500Base64", "^500Binary (ASCII)", "^500Binary (Integer)", "^500Hex (ASCII)", "^500Hex (Integer)", "^500Hex (JS)", "^500html", "^500Oct (ASCII).", "^500Oct (Integer).", "^500Oct (JS).", "^500Unicode", "^500URL^2000"],
                    typeSpeed: 80,
                    startDelay: 150,
                    backDelay: 500,
                    loop: true,
                });
            });
        });
</script>
<script src="assets/js/base64.js" type="text/javascript"></script>
<script src="assets/js/uri.js" type="text/javascript"></script>
<script type="text/javascript">
	$("#linkAbout").click(function() {
		$("#linkAbout").addClass('active');
		$("#linkBase64").removeClass('active');
		$("#linkUri").removeClass('active');
		$('#linkAbout').on('shown.bs.modal', function () {
		  $('#myInput').focus()
		})		
	});
	$("#linkBase64").click(function() {
		$("#linkBase64").addClass('active');
		$("#linkUri").removeClass('active');
		$("#linkAbout").removeClass('active');
		$("#base64").removeClass('hidden');
		$("#uri").addClass('hidden');
	});
	$("#linkUri").click(function() {
		$("#linkUri").addClass('active');
		$("#linkBase64").removeClass('active');
		$("#linkAbout").removeClass('active');
		$("#uri").removeClass('hidden');
		$("#base64").addClass('hidden');
	});
</script>
<!-- End Script -->
</body>
</html>